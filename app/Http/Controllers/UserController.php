<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
            "foo" => "bar",
            "bar" => "foo",
        ];
        dump('Here\'s dump message');
        dd('This is another way', $array);
    /* Debugbar::info($array);
       Debugbar::error('Error!');
       Debugbar::warning('Watch out…');
       Debugbar::addMessage('Another message', 'mylabel');*/

       return view('users.list',['user' =>'Bot'] );

    }  

}
